<h1>Login</h1>
<div class="wrap">
<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Email'); ?>
    <?php echo $form->input('email','email') ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->label('Password'); ?>
    <?php echo $form->input('password', 'password'); ?>
    <?php echo $form->error('password'); ?>


    <?php echo $form->submit('submitted', 'LOGIN'); ?>
</form>
</div>