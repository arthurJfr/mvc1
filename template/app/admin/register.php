

<div class="wrap">



<form action="" method="post" novalidate class="wrapform">
    <h1>Register</h1>


    <div class="email">
    <?php echo $form->label('Email'); ?>
    <?php echo $form->input('email','email') ?>
    <?php echo $form->error('email'); ?>
</div>
    <div class="pass">
    <?php echo $form->label('Password'); ?>
    <?php echo $form->input('password', 'password'); ?>

</div>
    <div class="confirmpass">
    <?php echo $form->label('Confirm password'); ?>
    <?php echo $form->input('password2', 'password'); ?>
    <?php echo $form->error('password'); ?>
</div>
    <div class="submit">
    <?php echo $form->submit('submitted', 'REGISTER'); ?>
    </div>
</form>
</div>
