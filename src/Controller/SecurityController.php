<?php

namespace App\Controller;

use App\Service\Form;
use App\Service\Validation;
use App\Service\Token;
use App\Model\UserModel;

use Core\Kernel\AbstractController;
use Core\Kernel\AbstractModel;



class SecurityController extends AbstractController
{
    public function login()
    {
        $errors = [];

        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $valid = new Validation();
            $errors['email'] = $valid->emailValid($post['email']);
            $errors['password'] = Validation::textValid($post['password'], 'mot de passe', 4, 50);

            $loginSelect =  UserModel::findByEmail('email',$post['email']);
            $this->dump($loginSelect);
            if (empty($loginSelect)){
                $errors['email'] = 'Identifiant  incorrect';
            }

            $passCheck = hash('md5',$post['password']);
            if ($passCheck !== $loginSelect->password){
                $errors['password'] = 'Mot de passe incorrect';
            }
            if ($valid->IsValid($errors)){

                AbstractController::redirect('admin-home');
            }




        }

        $formL = new Form($errors);
        $this->render('app.admin.login', array(
//            'message' => $message,

            'templateLogin' => 'yes',
            'form' => $formL
        ), 'admin');
    }


    public
    function register()
    {
        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $valid = new Validation();
            $errors['email'] = $valid->emailValid($post['email']);
            $verifemail = UserModel::findByEmail('email', $post['email']);
            if (!empty($verifemail)) {
                $errors['email'] = 'Cette adresse est déjà utilisée';
            }
            $errors['password'] = Validation::textValid($post['password'], 'mot de passe', 4, 50);
            $errors['password'] = Validation::textValid($post['password2'], 'mot de passe', 4, 50);
            if ($post['password'] !== $post['password2']) {
                $errors['password'] = 'Les mots de passes sont différents';
            }
            $hashPass = hash('md5', $post['password']);
            $this->dump($post);
            $this->dump($hashPass);
            if ($valid->IsValid($errors)) {

                echo $hashPass;
                $token = Token::generateRandString();

                UserModel::userInsert($post['email'], $hashPass, $token);

            }


        }
        $formR = new Form($errors);
        $this->render('app.admin.register', array(
//            'message' => $message,
            'templateRegister' => 'yes',
            'form' => $formR,
//            'paginator' => $paginator1
        ), 'admin');
    }
}