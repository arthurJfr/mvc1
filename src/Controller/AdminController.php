<?php

namespace App\Controller;




use JasonGrimes\Paginator;
use App\Model\AdminModel;
use Core\Kernel\AbstractController;

class AdminController extends AbstractController
{
    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';


        $totalItems = 1000;
        $itemsPerPage = 50;
        $currentPage = 8;
        $urlPattern = '/foo/page/(:num)';
        $paginator1 = new Paginator($totalItems,$itemsPerPage,$currentPage,$urlPattern);


        $this->render('app.admin.index',array(
            'message' => $message,
            'templateIndex' => 'yes',
            'paginator' => $paginator1
        ),'admin');

    }
}