<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;
use Core\Kernel\Database;
class UserModel extends AbstractModel
{
    public static function userInsert($email,$pass,$token){
        App::getDatabase()->prepareInsert(
            "INSERT INTO users (email,password,token,created_at) VALUES (?,?,?,NOW())",
            array($email, $pass, $token)
        );
    }
    public static function findByEmail($column,$value)
    {
        return App::getDatabase()->prepare("SELECT * FROM users WHERE ".$column." = ?",[$value],get_called_class(),true);
    }
}
